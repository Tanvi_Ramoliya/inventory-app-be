//category Model
module.exports = {
  attributes: {
    name: { type: "string" },
    description: { type: "string" },
    status: {
      type: "string",
      isIn: ["inactive", "active"],
      defaultsTo: "active",
    },
  },
  datastore: "mongodb",
};
