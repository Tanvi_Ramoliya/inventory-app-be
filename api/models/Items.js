/**
 * Items.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

//Item Model
module.exports = {
  attributes: {
    name: { type: "string", required: true },
    category: { type: "string", required: true },
    description: { type: "string", required: true },
    status: {
      type: "string",
      isIn: ["inactive", "active"],
      defaultsTo: "active",
    },
    price: { type: "number", defaultsTo: 0, columnType: "FLOAT" },
  },
  datastore: "mongodb",
};
