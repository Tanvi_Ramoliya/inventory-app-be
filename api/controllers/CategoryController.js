/**
 * CategoryController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  //get All Category Controller Logic
  list: function (req, res) {
    Category.find({}).sort('createdAt DESC').exec(function (err, categories) {
      if (err) {
        return res.status(500).send({ error: "Database Error!" });
      }
      return res.status(200).send({ categories: categories });
    });
  },
  //Create New Category Controller Logic
  create: function (req, res) {
    var name = req.body.name;
    var description = req.body.description;
    var status = req.body.status;

    Category.create({
      name: name,
      description: description,
      status: status,
    }).exec(function (err) {
      if (err) {
        return res.status(500).send({ error: "Database Error!" });
      }
      return res.status(200).send({
        message: "Successfully Category Added!",
        category: { name: name, description: description, status: status },
      });
    });
  },
  //Delete given Category Controller Logic
  delete: function (req, res) {
    Category.findOne({ id: req.params.id }).exec(function (err) {
      if (err) {
        return res.status(500).send({ error: "Database Error!" });
      }
      Category.destroy({ id: req.params.id }).exec(function (err) {
        if (err) {
          return res.status(500).send({ error: "Database Error!" });
        }
        return res
          .status(200)
          .send({ message: "Category Deleted!", categoryId: req.params.id });
      });
    });
  },
  //Update given Category Controller Logic
  update: function (req, res) {
    var name = req.body.name;
    var description = req.body.description;
    var status = req.body.status;
    Category.update(
      { _id: req.params.id },
      { name: name, description: description, status: status }
    ).exec(function (err) {
      if (err) {
        return res.status(500).send({ error: "Database Error!" });
      }
      return res.status(200).send({
        message: " Category Successfull Updated!",
        category: { name: name, description: description, status: status },
      });
    });
  },
};
