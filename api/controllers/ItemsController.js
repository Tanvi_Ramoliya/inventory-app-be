/**
 * ItemsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  //get All Items Controller Logic
  list: function (req, res) {
    Items.find({}).sort('createdAt DESC').exec(function (err, items) {
      if (err) {
        return res.status(500).send({ error: "Database Error!" });
      }
      return res.status(200).send({ items: items });
    });
  },
  //Craete New Item Controller Logic
  create: function (req, res) {
    var name = req.body.name;
    var category = req.body.category;
    var description = req.body.description;
    var status = req.body.status;
    var price = req.body.price;

    Items.create({
      name: name,
      description: description,
      status: status,
      category: category,
      price: price,
    }).exec(function (err) {
      if (err) {
        return res.status(500).send({ error: "Database Error!" });
      }
      return res.status(200).send({
        message: "Item Added Successfully!",
        Item: {
          name: name,
          description: description,
          status: status,
          category: category,
          price: price,
        },
      });
    });
  },
  //Delete Given Item Controller Logic
  delete: function (req, res) {
    Items.findOne({ id: req.params.id }).exec(function (err) {
      if (err) {
        return res.status(500).send({ error: "Database Error!" });
      }
      Items.destroy({ id: req.params.id }).exec(function (err) {
        if (err) {
          return res.status(500).send({ error: "Database Error!" });
        }
        return res
          .status(200)
          .send({ message: "Item Deleted!", itemId: req.params.id });
      });
    });
  },
  //Update Given Item Controller Logic
  update: function (req, res) {
    var name = req.body.name;
    var category = req.body.category;
    var description = req.body.description;
    var status = req.body.status;
    var price = req.body.price;
    Items.update(
      { _id: req.params.id },
      {
        name: name,
        description: description,
        status: status,
        category: category,
        price: price,
      }
    ).exec(function (err) {
      if (err) {
        return res.status(500).send({ error: "Database Error!" });
      }
      return res.status(200).send({
        message: " Item Successfully Updated!",
        item: {
          name: name,
          description: description,
          status: status,
          category: category,
          price: price,
        },
      });
    });
  },
};
